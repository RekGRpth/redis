#!/bin/sh

exec 2>&1
set -ex
test -f "$HOME/redis.conf" || cat >"$HOME/redis.conf" <<EOF
always-show-logo no
bind 0.0.0.0
port 6379
tcp-backlog $(cat /proc/sys/net/core/somaxconn)
EOF
exec redis-server "$HOME/redis.conf"
