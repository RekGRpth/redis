FROM alpine
RUN set -x \
    && apk add --no-cache --virtual .redis-rundeps \
        redis \
        shadow \
        su-exec \
        tzdata \
    && echo done
CMD [ "redis.sh" ]
COPY bin /usr/local/bin
ENTRYPOINT [ "docker_entrypoint.sh" ]
ENV HOME=/var/lib/redis
ENV GROUP=redis \
    USER=redis
VOLUME "${HOME}"
WORKDIR "${HOME}"
RUN set -ex \
    && chmod -R 0755 /usr/local/bin \
    && echo done
