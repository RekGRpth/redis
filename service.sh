#!/bin/sh -ex

#docker build --tag rekgrpth/redis .
#docker push rekgrpth/redis
docker pull rekgrpth/redis
docker network create --attachable --driver overlay docker || echo $?
docker volume create redis
docker service rm redis || echo $?
docker service create \
    --env GROUP_ID="$(id -g)" \
    --env LANG=ru_RU.UTF-8 \
    --env TZ=Asia/Yekaterinburg \
    --env USER_ID="$(id -u)" \
    --hostname tasks.redis \
    --mount type=bind,source=/etc/certs,destination=/etc/certs,readonly \
    --mount type=volume,source=redis,destination=/var/lib/redis \
    --name redis \
    --network name=docker \
    rekgrpth/redis
